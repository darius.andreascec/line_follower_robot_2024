// Definire pini pentru motoare
const int EN_A = 3;    // Pin pentru viteza motorului drept - cablu mov
const int IN_1 = 5;    // Pin pentru direcția motorului stâng - cablu albastru
const int IN_2 = 6;    // Pin pentru direcția motorului stâng - cablu verde
const int EN_B = 11;   // Pin pentru viteza motorului stâng - cablu maro
const int IN_3 = 9;    // Pin pentru direcția motorului stâng - cablu galben
const int IN_4 = 10;   // Pin pentru direcția motorului stâng - cablu portocaliu

// Definirea pinilor analogici
const int IR_stanga = A0;   // Senzorul IR din partea stângă - cablu alb
const int IR_central = A1;  // Senzorul IR din partea centrală - cablul negru
const int IR_dreapta = A2;  // Senzorul IR din partea dreaptă - cablu maro

// Definirea pragurilor de timp si viteză
const int CALIBRATION_TIME = 10000; // Timpul de calibrare în milisecunde
const int BASE_SPEED = 160; // Viteza de bază a motoarelor
const int TURN_SPEED = 110; // Viteza în timpul virajelor
const int STOP_SPEED = 0; // Viteza la oprirea robotului
const int LEFT_ADJUSTMENT = 24;  // Valoare pentru ajustarea vitezei motorului stâng

// Variabile de calibrare
int stangaCalibrare = 0;
int centralCalibrare = 0;
int dreaptaCalibrare = 0;
float threshold; // Pragul calculat dinamic în urma calibrării

void setup() {
  // Configurare pini pentru motoare
  pinMode(EN_A, OUTPUT);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(EN_B, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);

  // Configurare pini pentru senzori
  pinMode(IR_stanga, INPUT);
  pinMode(IR_central, INPUT);
  pinMode(IR_dreapta, INPUT);

  Serial.begin(9600);  // Inițializarea comunicației serială

  calibrareSenzori();  // Calibrarea senzorilor
}

void mers_inainte(int speed) 
{
  analogWrite(EN_A, speed);
  analogWrite(EN_B, speed);
  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
}

void viraj_stanga(int speed) 
{
  analogWrite(EN_A, speed);
  analogWrite(EN_B, speed);
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
}

void viraj_dreapta(int speed) 
{
  analogWrite(EN_A, speed);
  analogWrite(EN_B, speed);
  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
}

void oprire()
{
  analogWrite(EN_A, STOP_SPEED);
  analogWrite(EN_B, STOP_SPEED);
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, LOW);
}

void calibrareSenzori() 
{
    unsigned long startTime = millis();
    int contor_citiri = 0;

    int stangaTotal = 0;
    int centralTotal = 0;
    int dreaptaTotal = 0;

    Serial.println("Calibrare senzorii IR...");

    // Așteptare 10 secunde pentru calibrare
    while (millis() - startTime < CALIBRATION_TIME) 
    {
        int stanga = analogRead(IR_stanga);
        int central = analogRead(IR_central);
        int dreapta = analogRead(IR_dreapta);

        stangaTotal += stanga;
        centralTotal += central;
        dreaptaTotal += dreapta;

        contor_citiri++;

        // Printarea valorilor pentru debugging
        Serial.print("Stanga: "); Serial.print(stanga);
        Serial.print(" Central: "); Serial.print(central);
        Serial.print(" Dreapta: "); Serial.println(dreapta);

        delay(100); // Timp necesar pentru procesare
    }

    // Calcularea valorilor medii pentru calibrare
    stangaCalibrare = stangaTotal / contor_citiri;
    centralCalibrare = centralTotal / contor_citiri;
    dreaptaCalibrare = dreaptaTotal / contor_citiri;

    // Verificarea și ajustarea valorilor de calibrare pentru a nu fi negative
    stangaCalibrare = max(0, stangaCalibrare);
    centralCalibrare = max(0, centralCalibrare);
    dreaptaCalibrare = max(0, dreaptaCalibrare);

    // Calculul pragului
    threshold = (stangaCalibrare + centralCalibrare + dreaptaCalibrare) / 3.0;

    // Limitare prag
    threshold = max(threshold, 10.0);

    Serial.print("Prag dinamic calculat: ");
    Serial.println(threshold);

    Serial.print("Valorile de calibrare: ");
    Serial.print("Stânga: "); Serial.print(stangaCalibrare);
    Serial.print(", Central: "); Serial.print(centralCalibrare);
    Serial.print(", Dreapta: "); Serial.println(dreaptaCalibrare);
}

void citireSenzori(int &stanga, int &central, int &dreapta) 
{
  stanga = constrain(analogRead(IR_stanga) - stangaCalibrare, 0, 1023);
  central = constrain(analogRead(IR_central) - centralCalibrare, 0, 1023);
  dreapta = constrain(analogRead(IR_dreapta) - dreaptaCalibrare, 0, 1023);
}

void ajustare_viteza_motoare(int stanga, int dreapta) 
{
    int error = stanga - dreapta;
    int ajustare = map(error, -threshold, threshold, -20, 20); // Limitare prin ajustarea vitezei

    // Aplicăm o ajustare constantă pentru motorul stâng
    int viteza_motor_drept = BASE_SPEED + ajustare;
    int viteza_motor_stang = BASE_SPEED - ajustare + LEFT_ADJUSTMENT;

    // Asigurăm că vitezele sunt în intervalul PWM valid 0 - 255
    viteza_motor_drept = constrain(viteza_motor_drept, 0, 255);
    viteza_motor_stang = constrain(viteza_motor_stang, 0, 255);

    analogWrite(EN_A, viteza_motor_drept);
    analogWrite(EN_B, viteza_motor_stang);

    Serial.print("Viteză Dreapta: "); Serial.print(viteza_motor_drept);
    Serial.print(" | Viteză Stânga: "); Serial.println(viteza_motor_stang);
}

void loop() {
  // Citire valori senzor
  int stanga, central, dreapta;
  citireSenzori(stanga, central, dreapta);

  // Afișare valori senzor în consola serială
  Serial.print("Stanga: "); Serial.print(stanga);
  Serial.print(" Central: "); Serial.print(central);
  Serial.print(" Dreapta: "); Serial.println(dreapta);

  // Control motoare bazat pe senzori
  if (central < threshold && stanga < threshold && dreapta < threshold) 
  {
    oprire();
  }
  else if (central < threshold && stanga > threshold && dreapta > threshold) 
  {
    mers_inainte(BASE_SPEED - 60);
    //ajustare_viteza_motoare(stanga, dreapta);
  } 
  else if (stanga < threshold && central > threshold) 
  {
    viraj_dreapta(TURN_SPEED);
  } 
  else if (dreapta < threshold && central > threshold) 
  {
    viraj_stanga(TURN_SPEED);
  } 
  else if (stanga < threshold && central < threshold) 
  {
    viraj_dreapta(TURN_SPEED);
  } 
  else if (dreapta < threshold && central < threshold) 
  {
    viraj_stanga(TURN_SPEED);
  } 
  else {
    mers_inainte(BASE_SPEED - 60);
    ajustare_viteza_motoare(stanga, dreapta);
  }

  delay(100); // Timp necesar pentru stabilitate
}